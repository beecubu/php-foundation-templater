<?php

namespace Beecubu\Foundation\Templater\Exceptions;

use Exception;

/**
 * Quan no existeix el template.
 */
class TemplateNotFoundException extends Exception
{
    public function __construct(string $template, string $path)
    {
        parent::__construct("The template '{$template}' do not exists in '{$path}'.");
    }
}