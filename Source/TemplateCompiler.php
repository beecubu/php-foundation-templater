<?php

namespace Beecubu\Foundation\Templater;

use Beecubu\Foundation\Core\Exceptions\DateFormatterInvalidStyleException;
use Beecubu\Foundation\Core\Objectum;
use Beecubu\Foundation\Core\Tools\Formatters\DateFormatter;
use Beecubu\Foundation\Templater\Exceptions\TemplateNotFoundException;
use DateTime;

defined('TEMPLATES_PATH') or define('TEMPLATES_PATH', __DIR__.'/Resources/Templates/');
defined('TEMPLATES_GETTEXT_DOMAIN') or define('TEMPLATES_GETTEXT_DOMAIN', null);

/**
 * Compilador de templates.
 */
class TemplateCompiler
{
    private $paths = [TEMPLATES_PATH];

    // Class constructor

    /**
     * Retorna el compilador actual.
     *
     * @return static El compilador actual.
     */
    public static function current()
    {
        static $instance = null;
        // first time?
        if (is_null($instance))
        {
            $instance = new self();
        }
        // the new one
        return $instance;
    }

    // Private methods

    /**
     * Obté el path al template intentant carregar una versió localized.
     *
     * Funcionament:
     *  1) Primer prova de carregar una versió localized nativa (.phtml)
     *  2) Si no pot (1) llavors prova la versió localized normal (.html)
     *  3) Si no pot (2) llavors prova la versió no-localized nativa (.phtml)
     *  4) Si ni opt (3) llavors prova la versió no-localized normal (.html)
     *
     * @param string $templateName El nom del template.
     * @param string $locale L'idioma en que es vol el template.
     * @param array $data Les variables que s'utilitzen dins al template (necessari pels .phtml)
     *
     * @return string El contingut del template.
     *
     * @throws TemplateNotFoundException
     */
    private function localizedTemplateData(string $templateName, string $locale, array $data): string
    {
        // convert each . to /
        $templateName = str_replace('.', '/', $templateName);
        // look into each template path
        foreach ($this->paths as $path)
        {
            // localized phtml version
            if (file_exists($path.$templateName.".{$locale}.phtml"))
            {
                return $this->importNativeTemplate($path.$templateName.".{$locale}.phtml", $locale, $data);
            }
            // localized html version
            elseif (file_exists($path.$templateName.".{$locale}.html"))
            {
                return file_get_contents($path.$templateName.".{$locale}.html");
            }
            // non-localized phtml version
            elseif (file_exists($path.$templateName.'.phtml'))
            {
                return $this->importNativeTemplate($path.$templateName.'.phtml', $locale, $data);
            }
            // non-localized html version
            elseif (file_exists($path.$templateName.'.html'))
            {
                return file_get_contents($path.$templateName.'.html');
            }
        }
        // error!
        throw new TemplateNotFoundException($templateName, TEMPLATES_PATH);
    }

    /**
     * Carrega i executa un template natiu (PHP) i retorna l'output.
     *
     * @param string $filePath El path al fitxer PHP.
     * @param string $locale El codi de l'idioma.
     * @param array $data Les variables del template.
     *
     * @return string El contingut del template.
     */
    private function importNativeTemplate(string $filePath, string $locale, array $data): string
    {
        $prevLocale = getenv('LANG');
        // is the gettext feature enabled?
        if (TEMPLATES_GETTEXT_DOMAIN && $prevLocale !== $locale)
        {
            putenv('LANG='.$locale);
            // bind text domain
            bind_textdomain_codeset(TEMPLATES_GETTEXT_DOMAIN, 'UTF-8');
            textdomain(TEMPLATES_GETTEXT_DOMAIN);
        }
        // assign global template variables
        extract($data);
        // open buffer
        ob_start();
        // import PHP and execute
        require $filePath;
        // get the execution output
        $output = ob_get_contents();
        // finish and clean output flush
        ob_end_clean();
        // restore the previous locale
        if (TEMPLATES_GETTEXT_DOMAIN && $prevLocale !== $locale)
        {
            putenv('LANG='.$prevLocale);
            // bind text domain
            bind_textdomain_codeset(TEMPLATES_GETTEXT_DOMAIN, 'UTF-8');
            textdomain(TEMPLATES_GETTEXT_DOMAIN);
        }
        // return the executed code
        return $output;
    }

    /**
     * Evalúa una expressió i retorna el seu valor.
     *
     * @param string $expression La expressió a evaluar.
     * @param array $data El DataSet del template.
     * @param bool $compute TRUE = Processa el valor (l'array o DateTime), FALSE = retorna el valor tal qual.
     *
     * @return mixed El valor resultant de la avaluació.
     *
     * @throws DateFormatterInvalidStyleException
     */
    private function evaluate(string $expression, array $data, bool $compute = true)
    {
        $value = null;
        $propertyChain = explode('.', $expression);
        $propertyChainCount = count($propertyChain);
        // is the property defined into our $data??
        if (array_key_exists($propertyChain[0], $data) && ($value = $data[$propertyChain[0]]) !== null)
        {
            if ($value instanceof Objectum and $propertyChainCount > 1)
            {
                unset($propertyChain[0]);
                // get the "final" property value
                $value = $value->getNestedPropertyValue(implode('.', $propertyChain));
            }
            // is an array?
            if (is_array($value) && $compute)
            {
                $value = $value[$propertyChain[count($propertyChain) - 1]] ?? null;
            }
            // a transformation is needed?
            elseif ($value instanceof DateTime && $compute)
            {
                $value = DateFormatter::dateToString($value, 'es');
            }
        }
        return $value;
    }

    // Public methods

    /**
     * Afegeix un path on buscar els templates, i passa a ser el primer on mirar.
     *
     * @param string $path El nou path on mirar si hi ha templates.
     */
    public function appendTemplatesSearchPath(string $path): void
    {
        if ( ! in_array($path, $this->paths))
        {
            $this->paths = array_merge([$path], $this->paths);
        }
    }

    /**
     * Compila el template i el retorna l'html resultant.
     *
     * @param string $template El codi del template.
     * @param string $locale L'idioma en el que es vol el template.
     * @param array $data Les variables a substituir.
     * @param callable $result El el resultat de compilar el template. El callback és function(string $body, string? $subject).
     *
     * @throws TemplateNotFoundException
     *
     * @throws DateFormatterInvalidStyleException
     */
    public function compile(string $template, string $locale, array $data, callable $result): void
    {
        $subject = null;
        // parse includes
        while (preg_match("/@include\\s*\\(\\s*(s*.*?\\s*)\\s*\\)/i", $template, $matches))
        {
            // load the include template content
            $value = $this->localizedTemplateData($matches[1], $locale, $data);
            // scape text
            $match = str_replace('/', '\/', preg_quote($matches[1]));
            // replace the current match
            $template = preg_replace("/@include\\s*\\(\\s*".$match."\\s*\\)/i", $value, $template);
        }
        // parse foreach
        while (preg_match("/@foreach\s*\(\s*(.+?)\s*\)(.*?|\r)@end/mis", $template, $matches))
        {
            $foreachProcessed = '';
            // get the dataSet
            if ($dataSet = $this->evaluate($matches[1], $data, false) and is_array($dataSet))
            {
                // execute the foreach
                foreach ($dataSet as $index => $item)
                {
                    $foreachContent = $matches[2];
                    // replace each placeholder with real values
                    while (preg_match('/(@if\s*\(|{{)\s*(\$.*?)\s*(\)|}})/m', $foreachContent, $matches2))
                    {
                        $expanded = $matches2[1].str_replace('$', "{$matches[1]}.{$index}", $matches2[2]).$matches2[3];
                        // create the replace regex
                        $openTag = preg_quote($matches2[1]);
                        $closeTag = preg_quote($matches2[3]);
                        $regex = str_replace('$', '\\$', $matches2[2]);
                        // replace the "$." to "expanded form"
                        $foreachContent = preg_replace("/{$openTag}\\s*{$regex}\\s*{$closeTag}/i", $expanded, $foreachContent);
                    }
                    // append this new foreach iteration
                    $foreachProcessed .= $foreachContent;
                }
            }
            // replace the original foreach with this new processed content
            $template = str_replace($matches[0], $foreachProcessed, $template);
        }
        // parse conditionals if
        while (preg_match("/@if\s*\(\s*(.+?)\s*\)(.*?|\r)@fi/mis", $template, $matches))
        {
            $template = str_replace($matches[0], $this->evaluate($matches[1], $data) ? $matches[2] : '', $template);
        }
        // parse values
        while (preg_match("/{{\\s*(.*?)\\s*}}/i", $template, $matches))
        {
            $value = $this->evaluate($matches[1], $data);
            // scape text
            $match = str_replace('/', '\/', preg_quote($matches[1]));
            // replace the current match
            $template = preg_replace("/{{\\s*{$match}\\s*}}/i", $value ?? '', $template);
        }
        // parse string transformer: @nl2br(string)
        while (preg_match("/@nl2br\s*(\(.*(?1)?\))/i", $template, $matches))
        {
            $value = '';
            // is a valid "@nl2br" ??
            if ($params = str_getcsv(substr($matches[1], 1, -1)) and count($params) === 1)
            {
                $value = nl2br($this->evaluate($params[0], $data) ?? '');
            }
            // scape text
            $match = str_replace('/', '\/', preg_quote($matches[1]));
            // replace the current match
            $template = preg_replace("/@nl2br\s*{$match}/i", $value, $template);
        }
        // parse date transformer: @date(DateTime, 'locale', 'format')
        while (preg_match("/@date\s*(\(.*(?1)?\))/i", $template, $matches))
        {
            $value = '';
            // is a valid "@date" ??
            if ($params = str_getcsv(substr($matches[1], 1, -1)) and count($params) === 3)
            {
                if ($date = $this->evaluate($params[0], $data, false) and $date instanceof DateTime)
                {
                    $value = DateFormatter::dateToString($date, $params[1], $params[2]);
                }
            }
            // scape text
            $match = str_replace('/', '\/', preg_quote($matches[1]));
            // replace the current match
            $template = preg_replace("/@date\s*{$match}/i", $value, $template);
        }
        // parse subject (we do it after parse values because we allow to use {{ }} in subjects
        while (preg_match("/@subject\s*(\(.*(?1)?\))/i", $template, $matches))
        {
            $subject = substr($matches[1], 1, -1);
            // scape text
            $match = str_replace('/', '\/', preg_quote($matches[1]));
            // replace the current match
            $template = preg_replace("/@subject\s*{$match}/i", '', $template);
        }
        // return the template
        $result($template, $subject);
    }

    /**
     * Compila el template i el retorna l'html resultant.
     * Versió sense "callback", per un ús més simple.
     *
     * @param string $template El codi del template.
     * @param string $locale L'idioma en el que es vol el template.
     * @param array $data Les variables a substituir.
     * @param string|null $subject El subjecte resultant (si existeix).
     *
     * @return string
     *
     * @throws DateFormatterInvalidStyleException
     * @throws TemplateNotFoundException
     */
    public function compileStr(string $template, string $locale, array $data, ?string &$subject = null): string
    {
        $this->compile($template, $locale, $data, function (string $body, ?string $aSubject) use (&$result, &$subject) {
            $result = $body;
            $subject = $aSubject;
        });
        return $result;
    }

    /**
     * Llegeix de disc un template i el compila, retorna l'html resultant.
     *
     * @param string $templateName El nom del template.
     * @param string $locale L'idioma en el que es vol el template.
     * @param array $data Les variables a substituir.
     * @param callable $result El resultat de compilar el template. El callback és function(string $body, string? $subject).
     *
     * @throws DateFormatterInvalidStyleException
     * @throws TemplateNotFoundException
     */
    public function compileTemplate(string $templateName, string $locale, array $data, callable $result): void
    {
        $this->compile($this->localizedTemplateData($templateName, $locale, $data), $locale, $data, $result);
    }

    /**
     * Llegeix de disc un template i el compila, retorna l'html resultant.
     * Versió sense "callback", per un ús més simple.
     *
     * @param string $templateName El nom del template.
     * @param string $locale L'idioma en el que es vol el template.
     * @param array $data Les variables a substituir.
     * @param string|null $subject El subjecte resultant (si existeix).
     *
     * @return string
     *
     * @throws DateFormatterInvalidStyleException
     * @throws TemplateNotFoundException
     */
    public function compileTemplateStr(string $templateName, string $locale, array $data, ?string &$subject = null): string
    {
        $this->compileTemplate($templateName, $locale, $data, function (string $body, ?string $aSubject) use (&$result, &$subject) {
            $result = $body;
            $subject = $aSubject;
        });
        return $result;
    }
}
