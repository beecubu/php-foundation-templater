1.4.0
-----
- Afegits dos nous mètodes:
  - compileStr(...)
  - compileTemplateStr(...)

1.3.1
-----
- Millorat el suport a PHP 8.x

1.3.0
-----
- Actualitzades les llibreries internes

1.2.2
-----
- Solucionat el bug que feia que un native-template no es pogués fer servir dues vegades en una mateixa sessió

1.2.1
-----
- Afegit suport bàsic de configurar el Gettext

1.2.0
-----
- Implementat el suport als templates natius (.phtml)

1.1.0
-----
- Implementada la possibilitat d'afegir nous "paths" on buscar els templates. 

1.0.0
-----
Primeres versions de la llibrería.
